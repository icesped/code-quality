'use strict';

// This class is used for logins
class Login {

  constructor(hash) {

    this.sessions = [];
    this.users = [];
    this.passwords = [];

    Object.keys(hash).map(key => ({
            key, value: hash[key]
        }
    )).map(element => {
      this.users.push(element.key);
      this.passwords.push(element.value);
    });
  }

  /**
   * Logouts an user of the system.
   * @param user 
   */
  logout(user) {
    let index = this.sessions.indexOf(user)
    if (index > -1)
        this.sessions.splice(index, 1)
    else
        console.log("User not exist!")
  }

  /**
   * Checks if user exists
   * @param user 
   */
  userExists(user) {
    let index = this.users.indexOf(user)
    return (index > -1) ? true : false
  }

  /**
   * Register user
   * @param user 
   * @param password 
   */
  registerUser(user, password) {
    this.users.push(user);
    this.passwords.push(password);
  }

  /**
   * Delete user
   * @param user 
   */
  removeUser(user) {
    let index = this.users.indexOf(user)
    if (index > -1 ) {
        this.users.splice(index, 1)
        this.passwords.splice(index, 1)
    } else
        console.log("User not found!")
  }

  /**
   * Checks if password is correct
   * @param user 
   * @param password 
   */
  checkPassword(user, password) {
    let index = this.users.indexOf(user)
    return (index > -1 ) ? (this.passwords[index] === password) ? true : false : false
  }

 /**
  * Updates an user password
  * @param user 
  * @param oldPassword 
  * @param newPassword 
  */
  updatePassword(user, oldPassword, newPassword) {
    // First we check if the user exists
    let index = this.users.indexOf(user)

    if (index > -1) {
      if (this.passwords[index] === oldPassword) {
        this.passwords[index] = newPassword;
        return true;
      }
    }
    console.log("User not exists!")
    return false;
  }

  /**
   * Login an user in the system
   * @param user 
   * @param password 
   */
  login(user, password) {
    let index = this.users.indexOf(user)
    if (index > -1) 
        if (this.passwords[index] === password)
            this.sessions.push(user);
    else
        console.log("User not found!")
  }
}

let registeredUsers = {
  user1: 'pass1',
  user2: 'pass2',
  user3: 'pass3'
};

let login = new Login(registeredUsers);

login.registerUser('user4', 'pass4');
login.login('user4', 'pass4');
login.updatePassword('user3', 'pass3', 'pass5');
login.login('user3', 'pass5');
login.logout('user4');
login.logout('user3');